import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { UserService } from '../api/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  email: string;
  password: string;
  login$: Subscription;

  constructor(
    public router: Router,
    public userService: UserService,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.login$.unsubscribe();
  }

  login() {
    this.login$ = this.userService.checkLogin(this.email, this.password).subscribe(
      async isLogged => {
        if (isLogged) {
          await this.router.navigateByUrl("home");
        }
        else{
          const alert = await this.alertCtrl.create({
            header: "Alert",
            message: "L'email ou le mot de passe est invalide",
            buttons: ["OK"]
          });
          await alert.present();
        }
      }
    )
  }

}
