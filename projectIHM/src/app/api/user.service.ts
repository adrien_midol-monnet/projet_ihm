import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public httpApi: HttpClient
  ) { }

  checkLogin(email: string, password: string) : Observable<boolean> {
    console.log("Identifiant: " + email);
    console.log("Mot de passe: " + password);
    return this.httpApi.post('https://reqres.in/api/login', {
      email,
      password
    }).pipe(
      map((response: any) => !!response?.token),
      catchError(() => of(false))
    );;
  }

  getListUsers() : Observable<boolean> {
    return this.httpApi.get('https://reqres.in/api/users?page=2').pipe(
      map((response: any) => response?.data),
      catchError(() => of(false))
    );;
  }
  
  getUserDetail(id: number): Observable<any> {
    return this.httpApi.get(`https://reqres.in/api/users/${id}`).pipe(
      map((response: any) => response?.data)
    );
  }

}
