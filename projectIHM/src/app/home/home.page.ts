import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/api/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  userList$: Observable<any>;

  constructor(
    public router: Router,
    public userService: UserService
  ) {}
  
  async selectedUser(id: number) {
    sessionStorage.setItem('userId', String(id));
    await this.router.navigateByUrl('user-details');
  }
  
  ngOnInit() : void {
    this.userList$ = this.userService.getListUsers();
  }

  goCreate() : void {
    this.router.navigate(['/user-create'])
  }
}
