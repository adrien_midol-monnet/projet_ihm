import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {

  @Input()
  userList: any;

  @Output()
  selectedUser = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {}

}
