import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/api/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.page.html',
  styleUrls: ['./user-details.page.scss'],
})
export class UserDetailsPage implements OnInit {
  private selectedUserId: number;

  userDetail$: Observable<any>;

  constructor(
    public userService: UserService
  ) { }

  ngOnInit() {
    this.selectedUserId = Number(sessionStorage.getItem('userId'));
    this.userDetail$ = this.userService.getUserDetail(this.selectedUserId);
  }

}
