import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms'
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.page.html',
  styleUrls: ['./user-create.page.scss'],
})
export class UserCreatePage implements OnInit {

  ionicForm : FormGroup;
  profilePic : File;
  static get parameters() {
    return [[FormBuilder]];

  }
  constructor(public formBuilder : FormBuilder) {
    this.ionicForm= formBuilder.group({
      Fname:["", Validators.required],
      Lname:["", Validators.required],
      mail:["", Validators.required],
    });

    this.profilePic 
   }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  submitForm(){
    console.log(this.ionicForm.value )
    console.log( "picture added :"+ ((this.profilePic) != null))
  
  }

  loadImageFromDevice(event) {
    console.log(event);
    this.profilePic = event.target.files[0];
  }


}
