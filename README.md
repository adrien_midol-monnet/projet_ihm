# projet_IHM

Ce projet est réalisé par les étudiants suivants :
- Arnaud Guibert
- Alexandre Guillermin
- Rémi Huguenot
- Adrien Midol-Monnet

Sujet :
- Une page de login comme page principale
- Une page permettant de lister les utilisateurs
- Une page permettant d'accéder au détail d'un utilisateur
- Une page permettant de créer un utilisateur (disponible depuis la liste)

Optionnel: 
- Une page d'inscription disponible depuis la page de login
- Une page/modale/état permettant de modifier un utilisateur (disponible depuis la liste et le détail)
- Une modale/état permettant de supprimer un utilisateur (disponible depuis la liste et le détail)
- La possibilité de naviguer en arrière depuis l'application
- La possibilité de se déconnecter 
